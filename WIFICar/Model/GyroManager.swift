//
//  GyroManager.swift
//  WIFICar
//
//  Created by Anatolii Shevchenko on 10/21/18.
//  Copyright © 2018 Anatolii Shevchenko. All rights reserved.
//

import UIKit
import CoreMotion

protocol GyroManagerDelegate: AnyObject {
    func gyroManagerReceiveCommand(command: CarSendCommand, value: Int)
}

struct GyroManagerConstants {
    static let minRadian = 0.05
    static let maxRadian = 0.3
}

class GyroManager: NSObject {
    public weak var delegate: GyroManagerDelegate?
    
    public var isAvailable: Bool {
        return self.motionManager.isDeviceMotionAvailable
    }
    
    private var motionManager = CMMotionManager()
    private var isBaseMotionsInstalled = false
    private var baseMoveMotion = 0.0
    private var baseTurnMotion = 0.0

    private var lastMoveValue = CarCommandConstants.minValue - 1
    private var lastTurnValue = CarCommandConstants.minValue - 1
    
    func startUpdates() {
        self.motionManager.deviceMotionUpdateInterval = 0.1
        self.motionManager.showsDeviceMovementDisplay = true
        self.motionManager.startDeviceMotionUpdates(using: .xMagneticNorthZVertical,
                                                    to: OperationQueue.main,
                                                    withHandler: { (data, error) in
                                                        if let validData = data {
                                                            self.controlCarWithMotion(motion: validData)
                                                        }
        })
    }
    
    func stopUpdates() {
        self.motionManager.stopDeviceMotionUpdates()

        if let delegate = self.delegate {
            delegate.gyroManagerReceiveCommand(command: CarSendCommand.drive, value: 0)
            delegate.gyroManagerReceiveCommand(command: CarSendCommand.turn, value: 0)
        }

        self.isBaseMotionsInstalled = false
    }
    
    func controlCarWithMotion(motion: CMDeviceMotion) {
        let moveMotion = motion.attitude.roll
        let turnMotion = motion.attitude.pitch

        if !self.isBaseMotionsInstalled {
            self.baseMoveMotion = moveMotion
            self.baseTurnMotion = turnMotion
            self.isBaseMotionsInstalled = true
            return;
        }

        let moveMotionAdjusted = moveMotion - self.baseMoveMotion
        let turnMotionAdjusted = turnMotion - self.baseTurnMotion
        let radianDifference = GyroManagerConstants.maxRadian - GyroManagerConstants.minRadian

        var moveValue = 0
        var turnValue = 0

        let moveMotionAdjustedAbs = abs(moveMotionAdjusted) - GyroManagerConstants.minRadian
        if (moveMotionAdjustedAbs > 0) {
            moveValue = Int((moveMotionAdjustedAbs/radianDifference)*Double(CarCommandConstants.maxValue))
            moveValue = min(moveValue, CarCommandConstants.maxValue)
            moveValue = moveMotionAdjusted >= 0 ? moveValue : -moveValue
        }

        let turnMotionAdjustedAbs = abs(turnMotionAdjusted) - GyroManagerConstants.minRadian
        if (turnMotionAdjustedAbs > 0) {
            turnValue = Int((turnMotionAdjustedAbs/radianDifference)*Double(CarCommandConstants.maxValue))
            turnValue = min(turnValue, CarCommandConstants.maxValue)
            turnValue = turnMotionAdjusted >= 0 ? turnValue : -turnValue
        }

        if (UIApplication.shared.statusBarOrientation == .landscapeLeft) {
            moveValue = -moveValue
            turnValue = -turnValue
        }

        if (self.lastMoveValue != moveValue) {
            self.lastMoveValue = moveValue
            if let delegate = self.delegate { delegate.gyroManagerReceiveCommand(command: CarSendCommand.drive, value: moveValue) }
        }

        if (self.lastTurnValue != turnValue) {
            self.lastTurnValue = turnValue
            if let delegate = self.delegate { delegate.gyroManagerReceiveCommand(command: CarSendCommand.turn, value: turnValue) }
        }
    }
}
