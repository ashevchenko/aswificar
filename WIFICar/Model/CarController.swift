//
//  CarController.swift
//  WIFICar
//
//  Created by Anatolii Shevchenko on 10/20/18.
//  Copyright © 2018 Anatolii Shevchenko. All rights reserved.
//

import UIKit

struct Constants {
    static let host = "192.168.1.1"
    static let port = 1337
    static let connectTimeout = 4
    static let reconnectInterval = 4.0
    static let socketReadTimerInterval = 0.5
}

struct CarCommandConstants {
    static let minValue = -100
    static let zeroValue = 0
    static let maxValue = 100
}

enum CarReceiveCommand: String {
    case distance = "d"
    case responce = "r"
}

enum CarSendCommand: String {
    case drive = "d"
    case turn = "t"
    case light = "l"
}

protocol CarControllerDelegate: AnyObject {
    func carControllerDidConnect()
    func carControllerDidDisconnect()
    func carControllerDidFailToConnect(error: Error)
    func carControllerDidReceiveDistance(distance: Int)
    func carControllerDidReceiveCommandResponce(string: String)
    func carControllerDidReceiveUnknownResponce(string: String)
}

class CarController: NSObject {
    public weak var delegate: CarControllerDelegate?

    public func connect() {
        self.connectToSocket()
    }

    public func disconnect() {
        self.closeSocket()
        if let delegate = self.delegate { delegate.carControllerDidDisconnect() }
    }

    public func sendCommand(command: CarSendCommand, value: Int) {
        DispatchQueue.global(qos: .background).async {
            if let client = self.client {
                var safeValue = max(value, CarCommandConstants.minValue)
                safeValue = min(safeValue, CarCommandConstants.maxValue)
                
                let sendString = command.rawValue + String(safeValue)
                if let response = self.sendRequest(string: sendString, using: client) {
                    DispatchQueue.main.async { self.analyzeSocketResponse(string: response) }
                }
            }
        }
    }

    private var client: TCPClient?
    private var socketReadTimer: Timer?

    @objc private func connectToSocket() {
        DispatchQueue.global(qos: .background).async {
            self.client = TCPClient(address: Constants.host, port: Int32(Constants.port))
            guard let client = self.client else {
                DispatchQueue.main.async { self.failedToConnect() }
                return
            }

            switch client.connect(timeout: Constants.connectTimeout) {
            case .success:
                DispatchQueue.main.async {
                    if let delegate = self.delegate {
                        delegate.carControllerDidConnect()
                    }
                    self.socketReadTimer = Timer.scheduledTimer(timeInterval: Constants.socketReadTimerInterval,
                                                                target: self,
                                                                selector: #selector(self.socketReadTimerCall),
                                                                userInfo: nil,
                                                                repeats: true)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    if let delegate = self.delegate { delegate.carControllerDidFailToConnect(error: error) }
                    self.failedToConnect()
                }
            }
        }
    }

    private func closeSocket() {
        if let client = client {
            client.close()
        }
        if let socketReadTimer = socketReadTimer {
            socketReadTimer.invalidate()
        }
    }

    private func failedToConnect() {
        self.closeSocket()
        perform(#selector(connectToSocket), with: nil, afterDelay: Constants.reconnectInterval)
        if let delegate = self.delegate { delegate.carControllerDidFailToConnect(error: NSError(domain:"Close socket", code:0, userInfo:nil)) }
    }

    @objc func socketReadTimerCall() {
        guard let client = self.client else {
            failedToConnect()
            return
        }

        DispatchQueue.global(qos: .background).async {
            if let response = self.readResponse(from: client) {
                DispatchQueue.main.async { self.analyzeSocketResponse(string: response) }
            }
        }
    }

    private func sendRequest(string: String, using client: TCPClient) -> String? {
        switch client.send(string: string + "\r\n") {
        case .success:
            return readResponse(from: client)
        case .failure(let error):
            DispatchQueue.main.async {
                if let delegate = self.delegate {
                    delegate.carControllerDidFailToConnect(error: error)
                }
            }
            return nil
        }
    }

    private func readResponse(from client: TCPClient) -> String? {
        do {
            guard let response = try client.read(1024*10) else { return nil }
            return String(bytes: response, encoding: .utf8)
        } catch {
            DispatchQueue.main.async { self.failedToConnect() }
            return nil
        }
    }

    private func analyzeSocketResponse(string: String) {
        let receivedStrings = string.components(separatedBy: "\r\n")
        for string in receivedStrings {
            if string.isEmpty { continue };

            let index = string.index(string.startIndex, offsetBy:1)
            let command = String(string[..<index])
            let value = String(string[index...])

            switch command {
            case CarReceiveCommand.distance.rawValue:
                if let distance = Int(value) {
                    if let delegate = self.delegate {
                        delegate.carControllerDidReceiveDistance(distance: distance)
                    }
                }
            case CarReceiveCommand.responce.rawValue:
                if let delegate = self.delegate { delegate.carControllerDidReceiveCommandResponce(string: value) }
            default:
                if let delegate = self.delegate { delegate.carControllerDidReceiveUnknownResponce(string: value) }
            }
        }
    }
}
