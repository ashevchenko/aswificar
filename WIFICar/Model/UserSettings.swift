//
//  UserSettings.swift
//  WIFICar
//
//  Created by Anatolii Shevchenko on 10/21/18.
//  Copyright © 2018 Anatolii Shevchenko. All rights reserved.
//

import UIKit

struct UserSettingsConstants {
    static let logsKey = "logsKey"
    static let modeKey = "modeKey"
}

class UserSettings: NSObject {
    public class var isLogsEnabled: Bool {
        set { UserDefaults.standard.set(newValue, forKey: UserSettingsConstants.logsKey) }
        get { return UserDefaults.standard.bool(forKey: UserSettingsConstants.logsKey) }
    }
    public class var isGyroEnabled: Bool {
        set { UserDefaults.standard.set(newValue, forKey: UserSettingsConstants.modeKey) }
        get { return UserDefaults.standard.bool(forKey: UserSettingsConstants.modeKey) }
    }
}
