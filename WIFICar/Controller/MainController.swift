//
//  MainController.swift
//  WIFICar
//
//  Created by Anatolii Shevchenko on 10/12/18.
//  Copyright © 2018 Anatolii Shevchenko. All rights reserved.
//

import UIKit
import Foundation

class MainController: UIViewController, CarControllerDelegate, GyroManagerDelegate {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var lightsSwitch: UISwitch!
    @IBOutlet weak var lightsLabel: UILabel!

    @IBOutlet weak var distanceHeaderLabel: UILabel!
    @IBOutlet weak var distanceQuantityLabel: UILabel!
    @IBOutlet weak var distanceValueLabel: UILabel!

    @IBOutlet weak var gyroButton: UIButton!
    @IBOutlet weak var gyroTextLabel: UILabel!

    @IBOutlet weak var upButton: UIButton!
    @IBOutlet weak var downButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    private var carController = CarController()
    private lazy var gyroManager: GyroManager = { [unowned self] in
        let manager = GyroManager()
        manager.delegate = self
        return manager;
    }()
    
    public var isGyroscopeAvailable: Bool {
        return self.gyroManager.isAvailable
    }

    // MARK: view lifecycle and UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.enableControls(enable: false)
        self.enableGyroscopeMode(enable: false)

        self.carController.delegate = self
        self.carController.connect()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.textView.isHidden = !UserSettings.isLogsEnabled
        self.enableGyroscopeMode(enable: UserSettings.isGyroEnabled)
    }

    // MARK: IBActions
    @IBAction func onMenuButtonTap() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let settingsController = storyboard.instantiateViewController(withIdentifier: "SettingsController") as! SettingsController
        settingsController.mainController = self

        UIView.beginAnimations("animation", context: nil)
        UIView.setAnimationDuration(0.7)
        self.navigationController!.pushViewController(settingsController, animated: false)
        UIView.setAnimationTransition(UIView.AnimationTransition.flipFromLeft, for: self.navigationController!.view, cache: false)
        UIView.commitAnimations()
    }

    @IBAction func onFronButtonTap() {
        self.carController.sendCommand(command: CarSendCommand.drive, value: CarCommandConstants.maxValue)
    }

    @IBAction func onBackButtonTap() {
        self.carController.sendCommand(command: CarSendCommand.drive, value: CarCommandConstants.minValue)
    }

    @IBAction func onRightButtonTap() {
        self.carController.sendCommand(command: CarSendCommand.turn, value: CarCommandConstants.maxValue)
    }

    @IBAction func onLeftButtonTap() {
        self.carController.sendCommand(command: CarSendCommand.turn, value: CarCommandConstants.minValue)
    }

    @IBAction func onMoveButtonRelease() {
        self.carController.sendCommand(command: CarSendCommand.drive, value: CarCommandConstants.zeroValue)
    }

    @IBAction func onTurnButtonRelease() {
        self.carController.sendCommand(command: CarSendCommand.turn, value: CarCommandConstants.zeroValue)
    }

    @IBAction func ledSwitchValueChanged(mySwitch: UISwitch) {
        self.carController.sendCommand(command: CarSendCommand.light, value: mySwitch.isOn ? CarCommandConstants.maxValue : CarCommandConstants.zeroValue)
    }

    @IBAction func onGyroButtonTap() {
        self.gyroTextLabel.textColor = .clear
        self.gyroManager.startUpdates()
    }

    @IBAction func onGyroButtonRelease() {
        self.gyroTextLabel.textColor = .lightText
        self.gyroManager.stopUpdates()
    }

    // MARK: private methods
    private func setupUI() {
        self.downButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        self.leftButton.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
        self.rightButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))

        self.upButton.layer.cornerRadius = self.upButton.frame.height/2
        self.downButton.layer.cornerRadius = self.downButton.frame.height/2
        self.leftButton.layer.cornerRadius = self.leftButton.frame.height/2
        self.rightButton.layer.cornerRadius = self.rightButton.frame.height/2
    }

    private var isEnabledControls = false
    private func enableControls(enable : Bool) {
        self.isEnabledControls = enable

        self.lightsSwitch.isEnabled = enable
        self.lightsLabel.isEnabled = enable
        self.distanceHeaderLabel.isEnabled = enable
        self.distanceQuantityLabel.isEnabled = enable
        self.distanceValueLabel.isEnabled = enable

        let isGyroMode = UserSettings.isGyroEnabled
        let isEnableManual = enable && !isGyroMode
        let isEnableGyro = enable && isGyroMode

        self.enableManualControls(enable: isEnableManual)
        self.enableGyroControls(enable: isEnableGyro)
    }

    private func enableManualControls(enable : Bool) {
        self.upButton.isEnabled = enable
        self.downButton.isEnabled = enable
        self.leftButton.isEnabled = enable
        self.rightButton.isEnabled = enable
    }

    private func enableGyroControls(enable : Bool) {
        self.gyroTextLabel.isEnabled = enable
        self.gyroButton.isEnabled = enable
    }

    private func showManualControls(show : Bool) {
        self.upButton.isHidden = !show
        self.downButton.isHidden = !show
        self.leftButton.isHidden = !show
        self.rightButton.isHidden = !show
    }

    private func showGyroControls(show : Bool) {
        self.gyroTextLabel.isHidden = !show
        self.gyroButton.isHidden = !show
    }

    private func enableGyroscopeMode(enable : Bool) {
        self.showManualControls(show: !enable)
        self.enableManualControls(enable: !enable && self.isEnabledControls)

        self.showGyroControls(show: enable)
        self.enableGyroControls(enable: enable && self.isEnabledControls)
    }

    private func appendToLogTextField(string: String) {
        print(string)

        self.textView.text = self.textView.text.appending("\n\(string)")
        let bottom = NSMakeRange(self.textView.text.count - 1, 1)
        self.textView.scrollRangeToVisible(bottom)
    }

    // MARK: CarControllerDelegate
    func carControllerDidConnect() {
        self.enableControls(enable: true)
    }

    func carControllerDidDisconnect() {
        self.enableControls(enable: false)
    }

    func carControllerDidFailToConnect(error: Error) {
        self.enableControls(enable: false)
        self.appendToLogTextField(string: error.localizedDescription)
    }

    func carControllerDidReceiveDistance(distance: Int) {
        self.distanceValueLabel.text = String(distance);

        let isHidden = (distance == -1)
        self.distanceHeaderLabel.isHidden = isHidden
        self.distanceQuantityLabel.isHidden = isHidden
        self.distanceValueLabel.isHidden = isHidden
    }

    func carControllerDidReceiveCommandResponce(string: String) {
        self.appendToLogTextField(string: string)
    }

    func carControllerDidReceiveUnknownResponce(string: String) {
        self.appendToLogTextField(string: "Receive unknown command: " + string)
    }

    // MARK: GyroManagerDelegate
    func gyroManagerReceiveCommand(command: CarSendCommand, value: Int) {
        self.carController.sendCommand(command: command, value: value)
    }
}
