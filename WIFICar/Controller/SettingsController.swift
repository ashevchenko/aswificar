//
//  SettingsController.swift
//  WIFICar
//
//  Created by Anatolii Shevchenko on 10/14/18.
//  Copyright © 2018 Anatolii Shevchenko. All rights reserved.
//

import UIKit

class SettingsController: UIViewController {

    @IBOutlet weak var logsSwitch: UISwitch!
    @IBOutlet weak var modeSwitch: UISwitch!

    public var mainController: MainController!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.logsSwitch.isOn = UserSettings.isLogsEnabled
        self.modeSwitch.isOn = UserSettings.isGyroEnabled
        self.modeSwitch.isEnabled = self.mainController.isGyroscopeAvailable
    }

    // MARK: IBActions
    @IBAction func onMenuButtonTap() {
        if let navigationController = self.mainController.navigationController {
            UIView.beginAnimations("animation", context: nil)
            UIView.setAnimationDuration(0.7)
            navigationController.popViewController(animated: false)
            UIView.setAnimationTransition(UIView.AnimationTransition.flipFromRight, for: navigationController.view, cache: false)
            UIView.commitAnimations()
        }
    }

    @IBAction func logsSwitchChanged(mySwitch: UISwitch) {
        UserSettings.isLogsEnabled = mySwitch.isOn
    }

    @IBAction func modeSwitchChanged(mySwitch: UISwitch) {
        UserSettings.isGyroEnabled = mySwitch.isOn
    }
}
