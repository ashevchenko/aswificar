#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <Servo.h>

const char *ssid = "FreedomWIFI";
const char *password = "freedom23";

IPAddress serverIP(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);
WiFiServer server(1337);
Servo servo;

const byte trigPin = 5;
const byte echoPin = 0;
const byte ledPin = 2;
const byte lightsPin = 16;
const byte frontPin = 12;
const byte backPin = 13;
const byte turnPin = 4;

enum ReceiveCommandType {
  driveCommand = 'd',
  turnCommand = 't',
  lightCommand = 'l',
};

enum SendCommandType {
  distanceCommand = 'd',
  responceCommand = 'r',
};

const unsigned short sendDataOncePerLoopsCount = 200000;
const unsigned short distanceFromSensorToCarEdgeCm = 11;
const short motorStartValue = PWMRANGE/2.0;
const short servoAdjustAngle = 25;
const short maxCommandValue = 100;

void setup() 
{
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(lightsPin, OUTPUT);
  pinMode(frontPin, OUTPUT);
  pinMode(backPin, OUTPUT);
  
  digitalWrite(ledPin, HIGH);
  Serial.begin(115200);
  servo.attach(turnPin);
  turn(0);
  
  bool isSetConfig = WiFi.softAPConfig(serverIP, gateway, subnet);
  if (!isSetConfig) {
    showError(3);
  }
  
  bool isMakeWIFI = WiFi.softAP(ssid, password);
  if (!isMakeWIFI) {
    showError(4);
  }

  server.begin();
  Serial.println(WiFi.softAPIP());
}

void showError(byte errorCode)
{
  Serial.print("Error: ");
  Serial.println(errorCode);
  
  for (int j = 0; j < errorCode; j++) {
    blinkLED(100);
  }
  delay(300);
}

void blinkLED(byte halfInterval) {
  digitalWrite(ledPin, LOW);
  delay(halfInterval);
  digitalWrite(ledPin, HIGH);
  delay(halfInterval);
}

void drive(short value)
{
  short front = 0;
  short back = 0;
  
  if (value != 0) {
    short adjustedValue = ((float)value/maxCommandValue)*(PWMRANGE-motorStartValue);
  
    if (adjustedValue > 0) {
      front = adjustedValue + motorStartValue;
    } else if (adjustedValue < 0) {
      back = -adjustedValue + motorStartValue;
    }
  }
  
  analogWrite(frontPin, front);
  analogWrite(backPin, back);
}

void turn(short value)
{
  short turnAngle = 90 - ((float)value/maxCommandValue)*90;
  turnAngle = turnAngle - servoAdjustAngle;
  turnAngle = max(turnAngle, (short)0);
  servo.write(turnAngle);
}

void light(short value)
{
  analogWrite(lightsPin, value);
}

unsigned int currentDistance()
{
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  unsigned int impulseTime = pulseIn(echoPin, HIGH);
  unsigned int distance = impulseTime/58.2;

  return distance;
}

bool isDistanceSensorEnabled = true;
unsigned int sendDataCurrentLoop = sendDataOncePerLoopsCount;
void loop(void) 
{
  WiFiClient client = server.available();

  if (client) {
    Serial.println("Connected");

    while (client.connected()) {
      if (client.available()) {
        blinkLED(10);
        
        String receivedString = client.readStringUntil('\r\n');
        Serial.println(receivedString);

        char command = receivedString.charAt(0);
        String valueString = receivedString.substring(1, receivedString.length() - 1);
        int value = valueString.toInt();

        String responceMark = String((char)responceCommand);
        switch (command) {
          case driveCommand:
            drive(value);
            client.println(responceMark + "Drive(" + valueString + "): OK");
            break;

          case turnCommand:
            turn(value);
            client.println(responceMark + "Turn(" + valueString + "): OK");
            break;

          case lightCommand:
            light(value);
            client.println(responceMark + "Light(" + valueString + "): OK");
            break;

          default:
            client.println(responceMark + "Invalid command: " + receivedString);
            break;
        }
      }

      if (isDistanceSensorEnabled) {
        if (sendDataCurrentLoop == sendDataOncePerLoopsCount) {

          unsigned int distance = currentDistance();
          Serial.println(distance);

          if (distance > 0) {
            sendDataCurrentLoop = 0;
            int adjustedDistance = distance - distanceFromSensorToCarEdgeCm;
            if (adjustedDistance < 0) {
              adjustedDistance = 0;
            }
            client.println(String((char)distanceCommand) + String(adjustedDistance));
          }
          else {
            isDistanceSensorEnabled = false;
            client.println(String((char)distanceCommand) + String(-1));
            showError(2);
          }
        }
        else {
          sendDataCurrentLoop++;
        }
      }
    }
    isDistanceSensorEnabled = true;
    Serial.println("Disconnected");
    client.stop();
  } 
}
