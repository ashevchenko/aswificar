# ASWIFICar

Here we have two projects which allow you to make RC car and control it using WiFi from your iOS device:
1. WIFICar/WIFICar.xcodeproj - iOS side;
2. WIFICar/ESP8266WiFi/ESP8266WiFi.ino - arduino sketch for ESP8266 WiFi module.

Most recent code in master branch supports analog controlls.
Code inside feature/digital_controlls supports only digital controlls, i.e. enable-disable.